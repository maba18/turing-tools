# Awesome TurinTech [![Awesome](https://cdn.rawgit.com/sindresorhus/awesome/d7305f38d29fed78fa85652e3a63e154dd8e8829/media/badge.svg)](https://bitbucket.org/maba18/turing-tools) 

This repo will contain commonly used scripts and tools for all the programming languages used by TT people.


<!-- # Awesome Turing [![Awesome](https://cdn.rawgit.com/sindresorhus/awesome/d7305f38d29fed78fa85652e3a63e154dd8e8829/media/badge.svg)](https://github.com/sindresorhus/awesome) -->

A curated list of awesome frameworks, libraries, software and resources.


- [Awesome TurinTech ![Awesome](https://bitbucket.org/maba18/turing-tools)](#awesome-turintech-awesomehttpsbitbucketorgmaba18turing-tools)
  - [Python](#python)
    - [Logging Python](#logging-python)
    - [Exceptions Python](#exceptions-python)
  - [Machine Learning](#machine-learning)
    - [Anomaly Detection](#anomaly-detection)
    - [Data Visualisation](#data-visualisation)
  - [Evolutionary Optimisation](#evolutionary-optimisation)
    - [Evolutionary Optimisation](#evolutionary-optimisation-1)
 

---

## Python
*Libraries for python.*

This list contains different tools important for anyone writing code in Python.
* [python-logging](https://realpython.com/python-logging/) - Logging console in python tutorial.
* [python-exceptions](https://realpython.com/python-exceptions/) - Exceptions tutorial for Python3



### Logging Python

The logging module provides you with a default logger that allows you to get started without needing to do much configuration. The corresponding methods for each level can be called as shown in the following example:

code:

    import logging

    logging.debug('This is a debug message')
    logging.info('This is an info message')
    logging.warning('This is a warning message')
    logging.error('This is an error message')
    logging.critical('This is a critical message')

terminal:

    WARNING:root:This is a warning message
    ERROR:root:This is an error message
    CRITICAL:root:This is a critical message



By using the level parameter, you can set what level of log messages you want to record. This can be done by passing one of the constants available in the class, and this would enable all logging calls at or above that level to be logged. Here’s an example:

    import logging

    logging.basicConfig(level=logging.DEBUG)
    logging.debug('This will get logged')



Similarly, for logging to a file rather than the console, filename and filemode can be used, and you can decide the format of the message using format. The following example shows the usage of all three:

    import logging

    logging.basicConfig(filename='app.log', filemode='w', format='%(name)s - %(levelname)s - %(message)s')
    logging.warning('This will get logged to a file')



### Exceptions Python

    try:
        linux_interaction()
    except Exception as error:
        print(error)
    else:
        try:
            with open('file.log') as file:
                read_data = file.read()
        except Exception as fnf_error:
            print(fnf_error)



## Machine Learning

Below you will find a list of tools and code examples with ML related tasks

### Anomaly Detection

* [anomaly-detection-library](https://github.com/yzhao062/pyod/) - Very good library for anomaly detection.


### Data Visualisation

* [data-visualisation-ml](https://github.com/parrt/dtreeviz) - Good data visualisation tool to visualise tree based models.


## Evolutionary Optimisation
Sometimes we need to optimise different parameters to get the best performance of a model or code. Below we list common optimisation tools that we have used in the past.

### Evolutionary Optimisation
* [jmetal-for-python-libary](https://github.com/jMetal/jMetalPy) - A library to do parameter tuning for multiple objectives